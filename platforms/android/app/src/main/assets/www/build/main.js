webpackJsonp([1],{

/***/ 109:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 109;

/***/ }),

/***/ 150:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/voicerecording/voicerecording.module": [
		279,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 150;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_media__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_battery_status__ = __webpack_require__(275);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_media_capture__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_native_audio__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var MEDIA_FILES_KEY = 'mediaFiles';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, media, mediaCapture, storage, file, toastCtrl, batteryStatus, alertCtrl, platform, nativeAudio) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.media = media;
        this.mediaCapture = mediaCapture;
        this.storage = storage;
        this.file = file;
        this.toastCtrl = toastCtrl;
        this.batteryStatus = batteryStatus;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.nativeAudio = nativeAudio;
        this.playing = false;
        this.recording = false;
        this.audioList = [];
        this.audioRecording = {
            listening: 0
        };
        this.audioplaying = {
            listening: 0
        };
        this.timeViwerString = "";
        // listening:any;
        this.mediaFiles = [];
        this.subscription = this.batteryStatus.onChange().subscribe(function (status) {
            _this.batlvl = status.level;
        });
        if (this.batlvl < 20) {
            var alert_1 = this.alertCtrl.create({
                title: 'Low battery',
                subTitle: '20% of battery remaining',
                buttons: ['Dismiss']
            });
            alert_1.present();
        }
        console.log("listening", this.audioRecording);
    }
    HomePage.prototype.timer = function (arg0) {
        throw new Error("Method not implemented.");
    };
    HomePage.prototype.getAudioList = function () {
        if (localStorage.getItem("audiolist")) {
            this.audioList = JSON.parse(localStorage.getItem("audiolist"));
            console.log("audiolist", this.audioList);
        }
    };
    HomePage.prototype.ionViewWillEnter = function () {
        this.getAudioList();
    };
    HomePage.prototype.startRecord = function () {
        if (this.platform.is('ios')) {
            this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.3gp';
            this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
            this.audio = this.media.create(this.filePath);
        }
        else if (this.platform.is('android')) {
            this.storageDirectory = this.file.dataDirectory;
            this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.mp3';
            //  this.storage.get(MEDIA_FILES_KEY).then(x=>{
            //   this.filePath=this.fileName;
            // }) 
            //this.filePath = "/data/data/" + handler.cordova.getActivity().getPackageName() + "/cache/" + this.file;
            console.log(this.filePath);
            this.storageDirectory = this.file.externalDataDirectory.replace(/file:\/\//g, '');
            this.audio = this.media.create(this.storageDirectory + this.fileName);
            this.storedFile = this.storageDirectory + this.fileName;
            //alert(this.storageDirectory+this.fileName)
        }
        this.audio.startRecord();
        this.recording = true;
    };
    // setInterValTime() {
    //    setTimeout(() => {
    //     this.audioRecording.listening++;
    //     this.secondsToHms(this.audioRecording.listening)
    //     console.log("timer:", this.audioRecording.listening)
    //   }, 1000);
    // }
    HomePage.prototype.countOnDeactivate = function () {
        clearInterval(this.tempval);
    };
    HomePage.prototype.stopRecord = function () {
        this.countOnDeactivate();
        this.audio.stopRecord();
        //  let data = { filename: this.fileName };
        //  this.audioList.push(data);
        // localStorage.setItem("filePath", this.filePath);
        this.recording = false;
        //this.getAudioList();
    };
    HomePage.prototype.secondsToHms = function (d) {
        d = Number(d);
        var h = Math.floor(d / 3600);
        var m = Math.floor(d % 3600 / 60);
        var s = Math.floor(d % 3600 % 60);
        var hDisplay = h > 0 ? h + (h == 1 ? " hour " : " hours, ") : "";
        var mDisplay = m > 0 ? m + (m == 1 ? " : " : " : ") : "00:";
        var sDisplay = s > 0 ? s + (s == 1 ? " " : " ") : "00";
        this.timeViwerString = hDisplay + mDisplay + sDisplay;
    };
    HomePage.prototype.setInterValTime = function () {
        var _this = this;
        this.audioTimer = setInterval(function () {
            _this.audioplaying.listening++;
            _this.secondsToHms(_this.audioplaying.listening);
            console.log("timer:", _this.audioplaying.listening);
        }, 1000);
    };
    HomePage.prototype.setInterValTimeClear = function () {
        clearInterval(this.audioTimer);
        //this.audioplaying.listening=0;
    };
    HomePage.prototype.playAudio = function () {
        this.storageDirectory = this.file.dataDirectory;
        var duratin = this.audio.getDuration();
        //alert(duratin)
        this.setInterValTime();
        this.audio.play();
        this.playing = true;
    };
    HomePage.prototype.pauseAudio = function () {
        this.setInterValTimeClear();
        this.audio.pause();
        this.playing = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('audioPlayer'),
        __metadata("design:type", Object)
    ], HomePage.prototype, "audioPlayer", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/krishna/krishna/projects/VoiceRecording/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      <ion-item class="back-hdr">\n        <ion-avatar item-start>\n          <img src="assets/imgs/avatar-ts-woody.png">\n        </ion-avatar>\n        <h2>Steven J</h2>\n      </ion-item>\n\n    </ion-title>\n  </ion-navbar>\n  <div class="play-rec">\n    <!-- <button class="arrow-back">\n      <ion-icon name="arrow-back"></ion-icon>\n    </button> -->\n    <h1>\n      PLAY RECORDING</h1>\n  </div>\n  <!-- <div class="bat">{{batlvl}}%</div> -->\n</ion-header>\n\n<ion-content>\n  <h3>**Important**</h3>\n\n  <p>\n    Refer to your written discharge instructions for complete information and proper procedure. This recording contains a short\n    summary, not intended for final use. visit\' ABC Client for access to medical records, appointments and discharge instructions.\n  </p>\n\n\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-6>\n        Date:\n      </ion-col>\n      <ion-col class="date-styles" col-6>\n        03/12/18 - 3:15pm\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-12>\n        Discharge instructions\n        <!-- {{audioRecording.listening}} -->\n\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n  <ion-row>\n    <ion-col col-4></ion-col>\n    <ion-col col-1>\n      <button style="width:20px; height:20px; border-radius: 10px; background-color: red;" (click)="startRecord()" *ngIf="!recording"></button>\n      <button style="width:20px; height:20px; border-radius: 4px; background-color: red;" (click)="stopRecord()" *ngIf="recording">\n      </button>\n    </ion-col>\n    <ion-col col-5>\n      <div *ngIf="!recording" style="margin-top: 2px;">Record </div>\n      <div *ngIf="recording" style="margin-top: 2px;">Stop </div>\n    </ion-col>\n\n\n    <ion-col col-2></ion-col>\n\n\n  </ion-row>\n\n\n</ion-content>\n\n\n<ion-footer class="footer-back">\n  <ion-grid>\n\n\n    <ion-row>\n      <ion-col col-1>\n        <ion-icon class="pause-styles play-pause-styles" name="play" (click)="playAudio()" *ngIf="!playing"></ion-icon>\n        <ion-icon class="pause-styles play-pause-styles" name="pause" (click)="pauseAudio()" *ngIf="playing"></ion-icon>\n      </ion-col>\n      <!-- <ion-col col-2>\n      <ion-icon class="pause-styles play-pause-styles reset" id="reset"  name="square" (click)="resetTimer()"></ion-icon>\n      </ion-col> -->\n\n\n      <ion-col col-8 style="margin-left: 15px;">\n\n        <ion-range class="audio-range" [(ngModel)]="audioplaying.listening" style="height:30px !important;">\n\n        </ion-range>\n      </ion-col>\n      <!-- <ion-col col-8 style="margin-left: 15px;">\n        <audio controls (click)="playAudio()" #audioPlayer>\n          <source [src]="storedFile"  type="audio/mpeg"> Your browser does not support the audio element.\n        </audio>\n      </ion-col> -->\n      <!-- <ion-col col-2>\n            <div style="color: gray; font-size: 10px; margin-top:10px;">{{timeViwerString}}</div>\n          </ion-col> -->\n\n\n\n\n\n    </ion-row>\n  </ion-grid>\n\n\n  <ion-toolbar>\n    <ion-grid class="footer-styles">\n      <ion-row>\n        <ion-col col-1>\n\n          <ion-icon class="icon-styles" name="md-home"></ion-icon>\n        </ion-col>\n        <ion-col col-2 style="padding:6px 0 0 20px;">\n          <ion-icon class="icon-styles" name="help-circle"></ion-icon>\n        </ion-col>\n        <ion-col col-1></ion-col>\n        <ion-col col-5>\n          <ion-icon style="margin:6px 0 0 30px;" class="icon-styles plus-styles" name="add"></ion-icon>\n        </ion-col>\n        <ion-col col-1></ion-col>\n        <ion-col col-1>\n          <ion-icon class="icon-styles" name="create"></ion-icon>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/home/krishna/krishna/projects/VoiceRecording/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_4__ionic_native_battery_status__["a" /* BatteryStatus */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_media__["a" /* Media */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_media_capture__["a" /* MediaCapture */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_battery_status__["a" /* BatteryStatus */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_native_audio__["a" /* NativeAudio */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-list',template:/*ion-inline-start:"/home/krishna/krishna/projects/VoiceRecording/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/krishna/krishna/projects/VoiceRecording/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(223);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_media_capture__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_native_audio__ = __webpack_require__(198);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/voicerecording/voicerecording.module#VoicerecordingPageModule', name: 'VoicerecordingPage', segment: 'voicerecording', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_native_audio__["a" /* NativeAudio */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_media_capture__["a" /* MediaCapture */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_media__["a" /* Media */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 266:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_list_list__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            { title: 'List', component: __WEBPACK_IMPORTED_MODULE_5__pages_list_list__["a" /* ListPage */] }
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/krishna/krishna/projects/VoiceRecording/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/krishna/krishna/projects/VoiceRecording/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[200]);
//# sourceMappingURL=main.js.map