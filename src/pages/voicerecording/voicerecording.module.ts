import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VoicerecordingPage } from './voicerecording';

@NgModule({
  declarations: [
    VoicerecordingPage,
  ],
  imports: [
    IonicPageModule.forChild(VoicerecordingPage),
  ],
})
export class VoicerecordingPageModule {}
