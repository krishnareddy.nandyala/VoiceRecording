import { Component,ViewChild } from '@angular/core';
import { NavController, AlertController, Platform } from 'ionic-angular';
import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { BatteryStatus } from '@ionic-native/battery-status';
import { ToastController } from 'ionic-angular';
import { MediaCapture } from '@ionic-native/media-capture';
import { Storage } from '@ionic/storage';
import { NativeAudio } from '@ionic-native/native-audio';

const MEDIA_FILES_KEY = 'mediaFiles';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [BatteryStatus]
})
export class HomePage {
  @ViewChild('audioPlayer') audioPlayer: any;
  filePath: string;
  playing: boolean = false;

  timer(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  handle: any;

  recording: boolean = false;
  //filePath: string;
  fileName: string;
  audio: MediaObject;
  audioList: any[] = [];
  subscription: any;
  batlvl: number;
  audioRecording = {
    listening: 0
  }
  audioplaying= {
    listening: 0
  }
  timeViwerString = "";
  // listening:any;
  mediaFiles = [];
  constructor(public navCtrl: NavController, public media: Media, public mediaCapture: MediaCapture,
    private storage: Storage, public file: File,
    private toastCtrl: ToastController, private batteryStatus: BatteryStatus,
    private alertCtrl: AlertController, public platform: Platform, private nativeAudio: NativeAudio) {

    this.subscription = this.batteryStatus.onChange().subscribe
      (status => {
        this.batlvl = status.level
      });
    if (this.batlvl < 20) {
      let alert = this.alertCtrl.create({
        title: 'Low battery',
        subTitle: '20% of battery remaining',
        buttons: ['Dismiss']
      });
      alert.present();
    }
    console.log("listening", this.audioRecording);
  }
  getAudioList() {
    if (localStorage.getItem("audiolist")) {
      this.audioList = JSON.parse(localStorage.getItem("audiolist"));
      console.log("audiolist", this.audioList);
    }
  }
  ionViewWillEnter() {
    this.getAudioList();
  }


  startRecord() {
    
  

    if (this.platform.is('ios')) {
      this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.3gp';
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.storageDirectory = this.file.dataDirectory;
     
      this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.mp3';
      //  this.storage.get(MEDIA_FILES_KEY).then(x=>{
      //   this.filePath=this.fileName;
      // }) 
      //this.filePath = "/data/data/" + handler.cordova.getActivity().getPackageName() + "/cache/" + this.file;
      console.log(this.filePath);
      this.storageDirectory=this.file.externalDataDirectory.replace(/file:\/\//g, '');
      this.audio = this.media.create(this.storageDirectory+this.fileName);
      this.storedFile =this.storageDirectory+this.fileName;
      //alert(this.storageDirectory+this.fileName)
    }
    this.audio.startRecord()
    this.recording = true;

  }

  tempval;
 storedFile:any;
  // setInterValTime() {
  //    setTimeout(() => {

  //     this.audioRecording.listening++;
  //     this.secondsToHms(this.audioRecording.listening)
  //     console.log("timer:", this.audioRecording.listening)
  //   }, 1000);

  // }

  countOnDeactivate() {
    clearInterval(this.tempval)
  }


  stopRecord() {

    this.countOnDeactivate();
    this.audio.stopRecord();
  //  let data = { filename: this.fileName };
  //  this.audioList.push(data);
   // localStorage.setItem("filePath", this.filePath);
    this.recording = false;
    //this.getAudioList();

  }

  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " : " : " : ") : "00:";
    var sDisplay = s > 0 ? s + (s == 1 ? " " : " ") : "00";
    this.timeViwerString = hDisplay + mDisplay + sDisplay;
  }


  storageDirectory:any;


audioTimer;
  setInterValTime() {
   this.audioTimer = setInterval(() => {

     this.audioplaying.listening++;
     this.secondsToHms(this.audioplaying.listening)
     console.log("timer:", this.audioplaying.listening)
   }, 1000);
  }

  setInterValTimeClear()
  {
    clearInterval(this.audioTimer)
    //this.audioplaying.listening=0;
  }

  playAudio() {
     this.storageDirectory = this.file.dataDirectory;
     let duratin= this.audio.getDuration();
     //alert(duratin)
     this.setInterValTime();
     this.audio.play();
     this.playing = true;
  }
  pauseAudio(){
    this.setInterValTimeClear();
    this.audio.pause();
    this.playing = false;
  }


}
  